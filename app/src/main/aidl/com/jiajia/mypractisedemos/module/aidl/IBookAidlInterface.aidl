// IBookAidlInterface.aidl
package com.jiajia.mypractisedemos.module.aidl;

interface IBookAidlInterface {

    String getTitle();

    void setTitle(String title);

}